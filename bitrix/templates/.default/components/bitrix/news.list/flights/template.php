<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]) { ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? } ?>
    <? foreach ($arResult["ITEMS"] as $arItem) {
        $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
        ?>


        <div class="news-item" id="<?= $this->GetEditAreaId($arItem["ID"]); ?>">

            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]) { ?>
                <p class="news-item__title">
                    <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])) { ?>
                        <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a><br/>
                    <? } else { ?>
                        <? echo $arItem["NAME"] ?><br/>
                    <? } ?>
                </p>
            <? } ?>


            <div>
                <p><?= $arItem["PROPERTIES"]["USERS"]["NAME"] ?>:</p>
                <ul>
                    <? foreach ($arItem["PROPERTIES"]["USERS"]["VALUE"] as $authorId) { ?>
                        <li><? echo $arResult["AUTHORS"][$authorId]["LAST_NAME"] . " " . $arResult["AUTHORS"][$authorId]["NAME"] . " " . $arResult["AUTHORS"][$authorId]["SECOND_NAME"] ?></li>
                    <? } ?>
                </ul>
            </div>

            <div>
                <p><?= $arItem["PROPERTIES"]["AIRPORTS"]["NAME"] ?>:</p>
                <ul>
                    <?
                    foreach ($arItem["PROPERTIES"]["AIRPORTS"]["VALUE"] as $airportId) { ?>
                        <li>
                            <a href="<?= $arResult["AIRPORTS"][$airportId]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["AIRPORTS"][$airportId]["NAME"]; ?></a>
                        </li>
                    <? } ?>
                </ul>
            </div>


            <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["ACTIVE_FROM"]) { ?>
                <div>
                    <p>Дата изменения: <span class="news-date-time"><? echo $arItem["ACTIVE_FROM"] ?></span></p>
                </div>
            <? } ?>


        </div>
    <? } ?>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? } ?>
</div>
