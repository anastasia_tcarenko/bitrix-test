<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arrUsersId = array();
$arrAirportsId = array();

foreach ($arResult["ITEMS"] as $arItem) {
    $arrUsersId[] = $arItem["PROPERTIES"]["USERS"]["VALUE"];
    $arrAirportsId[] = $arItem["PROPERTIES"]["AIRPORTS"]["VALUE"];
}

/*ASSOCIATED USERS*/
$orderUser = false;
$filterUser = array(
    "ID" => $arrUsersId,
);

$elementsResult = CUser::GetList($orderUser, $filterUser);
while ($rsUser = $elementsResult->Fetch()) {
    $arResult["AUTHORS"][$rsUser["ID"]] = $rsUser;
}


/*ASSOCIATED ELEMENT*/
$arFilter = array(
    "IBLOCK_ID" => AIRPORTS_IBLOCK_ID,
    "ACTIVE" => "Y",
    "ID" => $arrAirportsId,
);
$arSelect = array("ID", "NAME", "DETAIL_PAGE_URL");
$BDRes = CIBlockElement::GetList(
    false,
    $arFilter,
    false,
    false,
    $arSelect
);

$arResult["AIRPORTS"] = array();
while ($arRes = $BDRes->GetNext()) {
    $arResult["AIRPORTS"][$arRes["ID"]] = $arRes;
}

