<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
<head>
    <? IncludeTemplateLangFile(__FILE__); ?>
    <?$APPLICATION->ShowHead();?>
    <? $APPLICATION->ShowMeta("keywords") ?>
    <? $APPLICATION->ShowMeta("description") ?>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title><? $APPLICATION->ShowTitle(false); ?></title>

    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/styles.css');
    $APPLICATION->SetAdditionalCSS('http://fonts.googleapis.com/css?family=Oswald:400,300');
    ?>

</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<div class="wrapper container">
    <header>
    </header>
    <div class="heading">
        <h1><? $APPLICATION->ShowTitle() ?></h1>
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
        "START_FROM" => 0,
        "PATH" => "",
        "SITE_ID" => SITE_ID
        ));?>
    </div>
    <div class="row">
        <section class="col-md-24">


