<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/flights/([0-9a-zA-Z_-]+)/?.*#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/flights/detail.php",
	),
	array(
		"CONDITION" => "#^/airports/([0-9]+)/?.*#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/airports/detail.php",
	),
	array(
		"CONDITION" => "#^/airports/?.*#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/airports/index.php",
	),
	array(
		"CONDITION" => "#^/flights/?.*#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/flights/index.php",
	),
);

?>